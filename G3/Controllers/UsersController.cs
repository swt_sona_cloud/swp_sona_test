﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Controller;

namespace G3.Controllers
{
    public class UsersController : Controller
    {
        private readonly SWPContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public UsersController(SWPContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        [Route("/Admin/UserList")]
        public async Task<IActionResult> Index()
        {
            var sWPContext = _context.Users.Include(u => u.DomainSetting).Include(u => u.RoleSetting);
            //var n = _context.Users.Count();
            return View("/Views/Users/Index.cshtml", await sWPContext.ToListAsync());
        }

        [Route("/Admin/Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
            ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
            var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
            ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
            return View("/Views/Users/Details.cshtml", user);
        }

        [HttpGet]
        [Route("/Admin/Create")]
        public IActionResult Create()
        {
            var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
            ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
            var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
            ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
            return View("/Views/Users/Create.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/Admin/Create")]
        public async Task<IActionResult> Create([Bind("Id,Email,DomainSettingId,RoleSettingId,Hash,Confirmed,Blocked,ConfirmToken,ConfirmTokenVerifyAt,ResetPassToken,Avatar,Name,DateOfBirth,Phone,Address,Gender,CreatedAt,UpdatedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                //Check if there is another user that use the same email.
                var isEmailInUse = _context.Users.Any(u => u.Email == user.Email);
                if (isEmailInUse)
                {
                    var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
                    ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
                    var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
                    ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
                    ViewData["ErrorMessage"] = new String("This email has been used.");
                    return View(user);
                }
                else
                {
                    IFormFile avatarFile = Request.Form.Files["AvatarFile"];
                    if (avatarFile != null && avatarFile.Length > 0)
                    {
                        string avatarPath = Path.Combine(_webHostEnvironment.WebRootPath, "avatars");

                        if (!Directory.Exists(avatarPath))
                        {
                            Directory.CreateDirectory(avatarPath);
                        }

                        string fileName = Guid.NewGuid().ToString() + Path.GetExtension(avatarFile.FileName);

                        string filePath = Path.Combine(avatarPath, fileName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await avatarFile.CopyToAsync(stream);
                        }
                        user.Avatar = "/avatars/" + fileName;
                    }
                    try
                    {
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UserExists(user.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
                ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
                var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
                ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
                return View(user);
            }

            return View("/Views/Users/Create.cshtml", user);
        }

        [HttpGet]
        [Route("/Admin/Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
            ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
            var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
            ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
            return View("/Views/Users/Edit.cshtml", user);
        }

        [HttpPost]
        [Route("/Admin/Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Email,DomainSettingId,RoleSettingId,Hash,Confirmed,Blocked,ConfirmToken,ConfirmTokenVerifyAt,ResetPassToken,Avatar,Name,DateOfBirth,Phone,Address,Gender,CreatedAt,UpdatedAt")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                //Check if there is another user that use the same email.
                var isEmailInUse = _context.Users.Any(u => u.Email == user.Email && u.Id != id);
                if (isEmailInUse)
                {
                    var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
                    ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
                    var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
                    ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
                    ViewData["ErrorMessage"] = new String("This email has been used.");
                    return View(user);
                }
                else
                {
                    IFormFile avatarFile = Request.Form.Files["AvatarFile"];
                    if (avatarFile != null && avatarFile.Length > 0)
                    {
                        string avatarPath = Path.Combine(_webHostEnvironment.WebRootPath, "avatars");

                        if (!Directory.Exists(avatarPath))
                        {
                            Directory.CreateDirectory(avatarPath);
                        }

                        string fileName = Guid.NewGuid().ToString() + Path.GetExtension(avatarFile.FileName);

                        string filePath = Path.Combine(avatarPath, fileName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await avatarFile.CopyToAsync(stream);
                        }
                        user.Avatar = "/avatars/" + fileName;
                    }
                    try
                    {
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UserExists(user.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index)); 
                }
            }
            else
            {
                var DomainSettings = _context.Settings.Where(ds => ds.Type == "DOMAIN").ToList();
                ViewData["DomainSettingId"] = new SelectList(DomainSettings, "SettingId", "Value");
                var RoleSettings = _context.Settings.Where(rs => rs.Type == "ROLE").ToList();
                ViewData["RoleSettingId"] = new SelectList(RoleSettings, "SettingId", "Value");
                return View(user); 
            }
        }

        [HttpPost]
        [Route("/Admin/Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'SWPContext.Users'  is null.");
            }
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
